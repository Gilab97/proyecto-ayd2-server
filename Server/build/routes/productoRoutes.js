"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const productoControllers_1 = require("../controllers/productoControllers");
class ProductoRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/update/:idUsuario/:idProducto', productoControllers_1.productoController.update);
        this.router.get('/listproduct', productoControllers_1.productoController.listproducts);
        this.router.post('/createProduct', productoControllers_1.productoController.create_product); // Crear Producto
        this.router.put('/deleteProducto/:idProducto', productoControllers_1.productoController.delete_product); // Eliminar Producto
        this.router.get('/listaProductoUnico/:idUsuario', productoControllers_1.productoController.lista_productoUnico); // Lista de productos
        this.router.get('/busquedaGlobal/:busqueda', productoControllers_1.productoController.busqueda_global); // Busqueda Global
        this.router.get('/listaProductoID/:idProducto', productoControllers_1.productoController.listaProductoID); // Listar Producto por ID
        this.router.get('/listaCategorias', productoControllers_1.productoController.lista_categorias); //Listar todas las categorias
        this.router.get('/busqueda/categoria/:id', productoControllers_1.productoController.busqueda_categoria); //Listar todas las categorias
    }
}
const productoRoutes = new ProductoRoutes();
exports.default = productoRoutes.router;
