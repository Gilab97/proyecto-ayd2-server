"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mensajesController_1 = require("../controllers/mensajesController");
class MesanjesRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/createSala', mensajesController_1.mensajesController.crearSalaChat); // Crear Sala
        this.router.post('/enviarMensaje', mensajesController_1.mensajesController.EnviarMensaje); // Enviar mensaje
    }
}
const mensajeRoutes = new MesanjesRoutes();
exports.default = mensajeRoutes.router;
