"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const reseniaControllers_1 = require("../controllers/reseniaControllers");
class ReseniaRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/crear-resenia', reseniaControllers_1.reseniaController.create_resenia); //Obtiene la Reseña
        this.router.get('/listaVendedores', reseniaControllers_1.reseniaController.vendedorResenia);
    }
}
const reseniaRoutes = new ReseniaRoutes();
exports.default = reseniaRoutes.router;
