"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.indexController = void 0;
const database_1 = __importDefault(require("../database"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
class IndexController {
    index(req, res) {
        database_1.default.query("SELECT * FROM Tipo_Usuario");
        res.json("Estamos conectados desde la base en AWS");
    }
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const correo = req.body.correo;
            const password = req.body.password;
            const rows = yield database_1.default.query('SELECT * from Market.Usuario ' +
                'Where correo_electronico = ? and contrasena = ?', [correo, password]);
            if (rows.length > 0) {
                const user = rows[0];
                const token = jsonwebtoken_1.default.sign({ _id: correo }, 'tokentest', {
                    expiresIn: 60 * 30
                });
                res.header('auth-token', token).json(user);
            }
            else {
                res.json({ "res": false });
            }
        });
    }
    colonias(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const rows = yield database_1.default.query('SELECT idColonia, nombre_residencial, nombre_colonia from Market.Colonia C INNER JOIN Market.Residencial R ON R.idResidencial = C.id_Residencial;');
            if (rows.length > 0) {
                res.json(rows);
            }
        });
    }
}
exports.indexController = new IndexController();
