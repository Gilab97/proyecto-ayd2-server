"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reseniaController = void 0;
const database_1 = __importDefault(require("../database"));
class ReseniaController {
    create_resenia(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(req.body);
            try {
                yield database_1.default.query('INSERT INTO Market.Resenia set ?', [req.body]);
                res.json({ text: 'Resenia Creado' });
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    vendedorResenia(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const rows = yield database_1.default.query('SELECT u.idUsuario, u.nombres, u.apellidos \n' +
                'FROM Usuario u, Resenia r, Tipo_Usuario t\n' +
                'WHERE t.idTipoUsuario = 1\n' +
                'AND u.tipo_usuario = t.idTipoUsuario\n' +
                'AND u.idUsuario = r.Usuario;');
            if (rows.length > 0) {
                res.json(rows);
            }
        });
    }
}
exports.reseniaController = new ReseniaController();
