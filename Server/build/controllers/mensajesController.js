"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.mensajesController = void 0;
const database_1 = __importDefault(require("../database"));
class MensajesController {
    crearSalaChat(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(req.body);
            let body = req.body;
            let idSala = body.idSalaChat;
            let user1 = body.Usuario1;
            let user2 = body.Usuario2;
            const rows = yield database_1.default.query('INSERT INTO Market.SalaChat(Usuario1,Usuario2) ' +
                'VALUES(\'' + user1 + '\',\'' + user2 + '\');');
            const rows2 = yield database_1.default.query('SELECT @@IDENTITY;');
            const rows3 = database_1.default.query('INSERT INTO Mensajes(Sala, Mensaje, Usuario1) VALUES(@@IDENTITY, \'Se Creo la Sala de Chat\', 1);');
            res.json({ text: 'Sala Creada' });
        });
    }
    EnviarMensaje(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(req.body);
            let body = req.body;
            let idSala = body.idSalaChat;
            let mensaje = body.mensaje;
            let user1 = body.user1;
            const rows = yield database_1.default.query('INSERT INTO Market.Mensajes(Sala,Mensaje,Usuario1) ' +
                'VALUES(' + idSala + ',\'' + mensaje + '\',' + user1 + ');');
            res.json({ text: 'Mensaje enviado' });
        });
    }
}
exports.mensajesController = new MensajesController();
