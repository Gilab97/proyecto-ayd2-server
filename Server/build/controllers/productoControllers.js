"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.productoController = void 0;
const aws_sdk_1 = __importDefault(require("aws-sdk"));
const aws_keys = require('../aws_keys');
const s3 = new aws_sdk_1.default.S3(aws_keys.s3);
const database_1 = __importDefault(require("../database"));
class ProductoController {
    // CAMBIOS PARA GUARDAR IMAGENES
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { idUsuario } = req.params;
                const { idProducto } = req.params;
                let body = req.body;
                let nombre = body.Nombre;
                let descripcion = body.descripcion;
                let precio = body.precio;
                let base64String = body.base64;
                let extension = body.extension;
                if (base64String != '') {
                    let date = new Date();
                    let str = date.getDate() + '_' + (date.getMonth() + 1) + '_' + date.getFullYear() + '_' + date.getHours() + '_' + date.getMinutes();
                    //Decodificar imagen
                    let encodedImage = base64String;
                    let decodedImage = Buffer.from(encodedImage, 'base64');
                    let filename = `${nombre}_${str}.${extension}`;
                    //Parámetros para S3
                    let bucketname = 'covidplace';
                    let folder = 'fotos/';
                    let filepath = `${folder}${filename}`;
                    var uploadParamsS3 = {
                        Bucket: bucketname,
                        Key: filepath,
                        Body: decodedImage,
                        ACL: 'public-read',
                    };
                    try {
                        yield s3.upload(uploadParamsS3, function async(err, data) {
                            if (err) {
                                console.log('Error uploading file:', err);
                                res.json({ "res": false });
                            }
                            else {
                                console.log('Upload success at:', data.Location);
                                const response = database_1.default.query('UPDATE Market.Producto SET ' +
                                    'Nombre = \'' + nombre + '\',' +
                                    'descripcion = \'' + descripcion + '\',' +
                                    'precio = ' + precio + ',' +
                                    'imagen = \'' + data.Location + '\' ' +
                                    'Where idUsuario = ' + idUsuario + ' and idProducto = ' + idProducto + ';');
                                res.json({ "res": true });
                            }
                        });
                    }
                    catch (e) {
                        console.log(e);
                    }
                }
                else {
                    const response = database_1.default.query('UPDATE Market.Producto SET ' +
                        'Nombre=\'' + nombre + '\',' +
                        'descripcion=\'' + descripcion + '\',' +
                        'precio=' + precio +
                        ' Where idUsuario = ' + idUsuario + ' and idProducto = ' + idProducto + ';');
                    res.json({ "res": true });
                }
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    listproducts(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const rows = yield database_1.default.query('SELECT p.Nombre, p.idProducto, p.descripcion, p.precio, p.imagen, u.idUsuario, u.nombres, u.apellidos, c.nombre_colonia, r.nombre_residencial'
                + ' FROM Market.Producto p, Market.Usuario u, Market.Colonia c, Market.Residencial r '
                + ' WHERE p.idUsuario = u.idUsuario '
                + ' and u.id_colonia = c.idColonia '
                + ' and c.id_Residencial = r.idResidencial;');
            if (rows.length > 0) {
                res.json(rows);
            }
            else {
                res.json({ "res": false });
            }
        });
    }
    //CREACION DE PRODUCTo
    create_product(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let body = req.body;
                let nombre = body.Nombre;
                let descripcion = body.Descripcion;
                let precio = body.Precio;
                let idUsuario = body.idUsuario;
                let base64String = body.base64;
                let extension = body.extension;
                if (base64String != '') {
                    let date = new Date();
                    let str = date.getDate() + '_' + (date.getMonth() + 1) + '_' + date.getFullYear() + '_' + date.getHours() + '_' + date.getMinutes();
                    //Decodificar imagen
                    let encodedImage = base64String;
                    let decodedImage = Buffer.from(encodedImage, 'base64');
                    let filename = `${nombre}_${str}.${extension}`;
                    //Parámetros para S3
                    let bucketname = 'covidplace';
                    let folder = 'fotos/';
                    let filepath = `${folder}${filename}`;
                    var uploadParamsS3 = {
                        Bucket: bucketname,
                        Key: filepath,
                        Body: decodedImage,
                        ACL: 'public-read',
                    };
                    try {
                        yield s3.upload(uploadParamsS3, function async(err, data) {
                            if (err) {
                                console.log('Error uploading file:', err);
                                res.send({ 'message': 'failed' });
                            }
                            else {
                                console.log('Upload success at:', data.Location);
                                const rows = database_1.default.query('INSERT INTO Market.Producto(Nombre,descripcion,precio,idUsuario,imagen) ' +
                                    'VALUES(\'' + nombre + '\',\'' + descripcion + '\',' + precio + ',' + idUsuario + ',\'' + data.Location + '\');');
                                res.json({ text: 'Produto Creado' });
                            }
                        });
                    }
                    catch (e) {
                        console.log(e);
                    }
                }
                else {
                    const rows = database_1.default.query('INSERT INTO Market.Producto(Nombre,descripcion,precio,idUsuario,imagen) ' +
                        'VALUES(\'' + nombre + '\',\'' + descripcion + '\',' + precio + ',' + idUsuario + ',\'\');');
                    res.json({ text: 'Produto Creado' });
                }
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    //ELIMINACION DE UN Producto
    delete_product(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { idProducto } = req.params;
            const response = yield database_1.default.query('DELETE from Market.Producto Where idProducto = ?', [idProducto]);
            res.json({ text: 'Producto Eliminado' });
        });
    }
    // VER Producto
    lista_productoUnico(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { idUsuario } = req.params;
            const users = yield database_1.default.query('SELECT * FROM  Market.Producto Where idUsuario= ?', [idUsuario]);
            res.json(users);
        });
    }
    //BUSQUEDA POR PRODUCTO CON EL ID
    listaProductoID(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { idProducto } = req.params;
            const rows = yield database_1.default.query('SELECT U.nombres, P.Nombre, P.descripcion, P.precio, P.imagen FROM Usuario AS U, Producto AS P Where idProducto = ? and P.idUsuario = U.idUsuario;', [idProducto]);
            if (rows.length > 0) {
                res.json(rows);
            }
            else {
                res.json({ "res": false });
            }
        });
    }
    lista_categorias(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const rows = yield database_1.default.query('SELECT * FROM  Market.CategoriaP;');
            if (rows.length > 0) {
                res.json(rows);
            }
            else {
                res.json({ "res": false });
            }
        });
    }
    busqueda_categoria(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const rows = yield database_1.default.query('SELECT p.idProducto, p.Nombre, p.descripcion, p.precio, p.idUsuario, p.imagen'
                + ' from Categoria_Producto cp, Producto p'
                + ' WHERE cp.idCategoriaP = ?'
                + ' and cp.idProducto = p.idProducto;', [id]);
            if (rows.length > 0) {
                res.json(rows);
            }
            else {
                res.json({ "res": false });
            }
        });
    }
    busqueda_global(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { busqueda } = req.params;
            const busq = yield database_1.default.query(`SELECT P.nombre, P.idProducto, P.descripcion, P.precio, P.idUsuario, P.imagen, U.nombres,  U.apellidos, C.nombre_colonia, R.nombre_residencial FROM Usuario as U, Producto as P, Colonia as C, Residencial as R WHERE P.descripcion LIKE "%${busqueda}%" AND R.idResidencial = C.id_Residencial AND C.idColonia = U.id_colonia AND U.idUsuario = P.idUsuario`);
            res.json(busq);
        });
    }
}
exports.productoController = new ProductoController();
