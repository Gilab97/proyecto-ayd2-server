"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const indexRoutes_1 = __importDefault(require("./routes/indexRoutes"));
const usuarioRoutes_1 = __importDefault(require("./routes/usuarioRoutes"));
const productoRoutes_1 = __importDefault(require("./routes/productoRoutes"));
const mensajesRoute_1 = __importDefault(require("./routes/mensajesRoute"));
const reseniaRoutes_1 = __importDefault(require("./routes/reseniaRoutes"));
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
    }
    config() {
        this.app.set('port', process.env.PORT || 3000);
        this.app.use(morgan_1.default('dev'));
        this.app.use(express_1.default.json({ limit: '50mb' }));
        this.app.use(express_1.default.urlencoded({ extended: false }));
        // this.app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }));
        this.app.use(cors_1.default());
    }
    routes() {
        this.app.use('/api', indexRoutes_1.default);
        this.app.use('/api/user', usuarioRoutes_1.default);
        this.app.use('/api/product', productoRoutes_1.default);
        this.app.use('/api/sala', mensajesRoute_1.default);
        this.app.use('/api/resenia', reseniaRoutes_1.default);
    }
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log('Server on port', this.app.get('port'));
        });
    }
}
const server = new Server();
server.start();
