import { Router } from 'express';

import  { productoController } from '../controllers/productoControllers'

class ProductoRoutes {

    public router: Router = Router();

   constructor() {
    this.config();
   }

   config(): void {
       this.router.post('/update/:idUsuario/:idProducto' , productoController.update);
       this.router.get('/listproduct', productoController.listproducts);
       this.router.post('/createProduct', productoController.create_product); // Crear Producto
       this.router.put('/deleteProducto/:idProducto',productoController.delete_product); // Eliminar Producto
       this.router.get('/listaProductoUnico/:idUsuario',productoController.lista_productoUnico); // Lista de productos
       this.router.get('/busquedaGlobal/:busqueda',productoController.busqueda_global); // Busqueda Global
       this.router.get('/listaProductoID/:idProducto',productoController.listaProductoID); // Listar Producto por ID
       this.router.get('/listaCategorias', productoController.lista_categorias); //Listar todas las categorias
       this.router.get('/busqueda/categoria/:id', productoController.busqueda_categoria); //Listar todas las categorias
   }
}

const productoRoutes = new ProductoRoutes();
export default productoRoutes.router;