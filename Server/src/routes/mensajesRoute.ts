import { Router } from 'express';

import  { mensajesController } from '../controllers/mensajesController'

class MesanjesRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.post('/createSala', mensajesController.crearSalaChat); // Crear Sala
        this.router.post('/enviarMensaje', mensajesController.EnviarMensaje); // Enviar mensaje

    }
}

const mensajeRoutes = new MesanjesRoutes();
export default mensajeRoutes.router;