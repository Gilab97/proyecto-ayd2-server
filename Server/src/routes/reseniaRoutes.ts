import { Router } from 'express';

import  { reseniaController } from '../controllers/reseniaControllers'

class ReseniaRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.post('/crear-resenia', reseniaController.create_resenia); //Obtiene la Reseña
        this.router.get('/listaVendedores', reseniaController.vendedorResenia);
    }
}

const reseniaRoutes = new ReseniaRoutes();
export default reseniaRoutes.router;