import {Request, Response} from 'express';

import pool from '../database'
import jwt from "jsonwebtoken"

class ReseniaController {

    public async create_resenia (req: Request, res:Response):Promise<void>{
        console.log(req.body);

        try {
            await pool.query('INSERT INTO Market.Resenia set ?',[req.body]);

            res.json({text: 'Resenia Creado'});

        } catch (e) {
            console.log(e);
        }
    }

    public async vendedorResenia (req: Request, res: Response){
        const rows = await pool.query('SELECT u.idUsuario, u.nombres, u.apellidos \n' +
            'FROM Usuario u, Resenia r, Tipo_Usuario t\n' +
            'WHERE t.idTipoUsuario = 1\n' +
            'AND u.tipo_usuario = t.idTipoUsuario\n' +
            'AND u.idUsuario = r.Usuario;');
        if(rows.length > 0)
        {
            res.json(rows);
        }
    }

}

export const reseniaController = new ReseniaController();