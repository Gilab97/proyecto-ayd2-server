import {Request, Response} from 'express';

import pool from '../database'

class MensajesController {

    public async crearSalaChat(req: Request, res: Response): Promise<void> {
        console.log(req.body);

        let body = req.body;
        let idSala = body.idSalaChat;
        let user1 = body.Usuario1;
        let user2 = body.Usuario2;

        const rows = await pool.query('INSERT INTO Market.SalaChat(Usuario1,Usuario2) ' +
            'VALUES(\'' + user1 + '\',\'' + user2 + '\');');
        const rows2 = await pool.query('SELECT @@IDENTITY;');

        const rows3 = pool.query('INSERT INTO Mensajes(Sala, Mensaje, Usuario1) VALUES(@@IDENTITY, \'Se Creo la Sala de Chat\', 1);');

        res.json({text: 'Sala Creada'});
    }

    public async EnviarMensaje(req: Request, res: Response): Promise<void> {
        console.log(req.body);
        let body = req.body;
        let idSala = body.idSalaChat;
        let mensaje = body.mensaje;
        let user1 = body.user1;
        const rows = await pool.query('INSERT INTO Market.Mensajes(Sala,Mensaje,Usuario1) ' +
            'VALUES(' + idSala + ',\'' + mensaje + '\',' + user1 + ');');

        res.json({text: 'Mensaje enviado'});
    }

}
export const mensajesController = new MensajesController();
