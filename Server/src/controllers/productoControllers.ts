import {Request, Response} from 'express';
import AWS, {AWSError} from 'aws-sdk'
import {ManagedUpload} from "aws-sdk/lib/s3/managed_upload";
import SendData = ManagedUpload.SendData;

const aws_keys = require('../aws_keys');
const s3 = new AWS.S3(aws_keys.s3);
import pool from '../database'

class ProductoController {

    // CAMBIOS PARA GUARDAR IMAGENES
    public async update(req: Request, res: Response) {
        try {
            const {idUsuario} = req.params;
            const {idProducto} = req.params;

            let body = req.body;
            let nombre = body.Nombre;
            let descripcion = body.descripcion;
            let precio = body.precio;
            let base64String = body.base64;
            let extension = body.extension;


            if (base64String != '') {
                let date = new Date();
                let str = date.getDate() + '_' + (date.getMonth() + 1) + '_' + date.getFullYear() + '_' + date.getHours() + '_' + date.getMinutes()

                //Decodificar imagen
                let encodedImage = base64String;
                let decodedImage = Buffer.from(encodedImage, 'base64');
                let filename = `${nombre}_${str}.${extension}`;

                //Parámetros para S3
                let bucketname = 'covidplace';
                let folder = 'fotos/';
                let filepath = `${folder}${filename}`;
                var uploadParamsS3 = {
                    Bucket: bucketname,
                    Key: filepath,
                    Body: decodedImage,
                    ACL: 'public-read',
                };


                try {
                    await s3.upload(uploadParamsS3, function async(err: Error, data: SendData) {
                        if (err) {
                            console.log('Error uploading file:', err);
                            res.json({"res":false});
                        } else {
                            console.log('Upload success at:', data.Location);
                            const response = pool.query('UPDATE Market.Producto SET ' +
                                'Nombre = \'' + nombre + '\',' +
                                'descripcion = \'' + descripcion + '\',' +
                                'precio = ' + precio + ',' +
                                'imagen = \'' + data.Location + '\' ' +
                                'Where idUsuario = '+idUsuario+' and idProducto = '+idProducto+';');
                                 res.json({"res":true});

                        }
                    });
                } catch (e) {
                    console.log(e);
                }
            } else {
                const response =  pool.query('UPDATE Market.Producto SET ' +
                    'Nombre=\'' + nombre + '\',' +
                    'descripcion=\'' + descripcion + '\',' +
                    'precio=' + precio +
                    ' Where idUsuario = '+idUsuario+' and idProducto = '+idProducto+';')
                    res.json({"res":true});
            }
        } catch (e) {
            console.log(e);
        }
    }


    public async listproducts(req: Request, res: Response) {
        
        const rows = await pool.query('SELECT p.Nombre, p.idProducto, p.descripcion, p.precio, p.imagen, u.idUsuario, u.nombres, u.apellidos, c.nombre_colonia, r.nombre_residencial'
                                    +' FROM Market.Producto p, Market.Usuario u, Market.Colonia c, Market.Residencial r '
                                    +' WHERE p.idUsuario = u.idUsuario '
                                    +' and u.id_colonia = c.idColonia '
                                    +' and c.id_Residencial = r.idResidencial;');

        if( rows.length > 0){
            res.json(rows); 
        }else{
            res.json({"res":false}); 
        }
    }

    //CREACION DE PRODUCTo
    public async create_product (req: Request, res:Response){
        try {
            let body = req.body;
            let nombre = body.Nombre;
            let descripcion = body.Descripcion;
            let precio = body.Precio;
            let idUsuario = body.idUsuario
            let base64String = body.base64;
            let extension = body.extension;


            if (base64String != '') {
                let date = new Date();
                let str = date.getDate() + '_' + (date.getMonth() + 1) + '_' + date.getFullYear() + '_' + date.getHours() + '_' + date.getMinutes()

                //Decodificar imagen
                let encodedImage = base64String;
                let decodedImage = Buffer.from(encodedImage, 'base64');
                let filename = `${nombre}_${str}.${extension}`;

                //Parámetros para S3
                let bucketname = 'covidplace';
                let folder = 'fotos/';
                let filepath = `${folder}${filename}`;
                var uploadParamsS3 = {
                    Bucket: bucketname,
                    Key: filepath,
                    Body: decodedImage,
                    ACL: 'public-read',
                };


                try {
                    await s3.upload(uploadParamsS3, function async(err: Error, data: SendData) {
                        if (err) {
                            console.log('Error uploading file:', err);
                            res.send({'message': 'failed'})
                        } else {
                            console.log('Upload success at:', data.Location);
                            const rows = pool.query('INSERT INTO Market.Producto(Nombre,descripcion,precio,idUsuario,imagen) ' +
                                'VALUES(\'' + nombre + '\',\'' + descripcion + '\',' + precio + ',' + idUsuario + ',\'' + data.Location + '\');');

                            res.json({text: 'Produto Creado'});

                        }
                    });
                } catch (e) {
                    console.log(e);
                }
            } else {
                const rows = pool.query('INSERT INTO Market.Producto(Nombre,descripcion,precio,idUsuario,imagen) ' +
                    'VALUES(\'' + nombre + '\',\'' + descripcion + '\',' + precio + ',' + idUsuario + ',\'\');');

                res.json({text: 'Produto Creado'});
            }

        }catch (e) {
            console.log(e);
        }
    }

//ELIMINACION DE UN Producto

    public async delete_product(req: Request, res: Response):Promise<void> {

        const { idProducto } = req.params;
        const response = await pool.query('DELETE from Market.Producto Where idProducto = ?', [idProducto]);
        res.json({text: 'Producto Eliminado'});
    }

// VER Producto

    public async lista_productoUnico(req: Request, res:Response):Promise<void>{
        const { idUsuario } = req.params;

        const users= await pool.query('SELECT * FROM  Market.Producto Where idUsuario= ?', [idUsuario]);
        res.json(users);
    }

    //BUSQUEDA POR PRODUCTO CON EL ID
    public async listaProductoID(req: Request, res: Response) {
        const { idProducto } = req.params;
        const rows = await pool.query('SELECT U.nombres, P.Nombre, P.descripcion, P.precio, P.imagen FROM Usuario AS U, Producto AS P Where idProducto = ? and P.idUsuario = U.idUsuario;',[idProducto]);

        if( rows.length > 0){
            res.json(rows);
        }else{
            res.json({"res":false});
        }
    }

    public async lista_categorias(req: Request, res:Response):Promise<void>{

        const rows = await pool.query('SELECT * FROM  Market.CategoriaP;');
        if( rows.length > 0){
            res.json(rows);
        }else{
            res.json({"res":false});
        }
    }

    public async busqueda_categoria(req: Request, res:Response):Promise<void>{

        const {id} = req.params;
        const rows = await pool.query('SELECT p.idProducto, p.Nombre, p.descripcion, p.precio, p.idUsuario, p.imagen'
                                        +' from Categoria_Producto cp, Producto p'
                                        +' WHERE cp.idCategoriaP = ?'
                                        +' and cp.idProducto = p.idProducto;', [id]);
        if(rows.length > 0){
            res.json(rows);
        }else{
            res.json({"res":false});
        }
    }
    public async busqueda_global(req: Request, res:Response):Promise<void>{
        const {busqueda} = req.params;
        const busq= await pool.query(`SELECT P.nombre, P.idProducto, P.descripcion, P.precio, P.idUsuario, P.imagen, U.nombres,  U.apellidos, C.nombre_colonia, R.nombre_residencial FROM Usuario as U, Producto as P, Colonia as C, Residencial as R WHERE P.descripcion LIKE "%${busqueda}%" AND R.idResidencial = C.id_Residencial AND C.idColonia = U.id_colonia AND U.idUsuario = P.idUsuario`);
        res.json(busq);
    }
}

export const productoController = new ProductoController();

