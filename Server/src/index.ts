import express, {Application} from 'express';
import morgan from 'morgan'
import cors from 'cors';
import bodyParser from "body-parser";

import indexRoutes from './routes/indexRoutes'
import usuarioRoutes from './routes/usuarioRoutes'
import productoRoutes from './routes/productoRoutes'
import mensajesRoutes from './routes/mensajesRoute'
import reseniaRoutes from './routes/reseniaRoutes'

class Server{

    public app: Application;

    constructor(){
        this.app = express();
        this.config();
        this.routes();
    }

    config(): void{
        this.app.set('port', process.env.PORT || 3000);
        this.app.use(morgan('dev'));
        this.app.use(express.json({limit: '50mb'}));
        this.app.use(express.urlencoded({extended: false}));
       // this.app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }));
        this.app.use(cors());    }

    routes(): void{
        this.app.use('/api', indexRoutes);
        this.app.use('/api/user', usuarioRoutes);
        this.app.use('/api/product', productoRoutes);
        this.app.use('/api/sala', mensajesRoutes);
        this.app.use('/api/resenia', reseniaRoutes);
    }

    start(): void{
        this.app.listen(this.app.get('port'), () =>{
            console.log('Server on port', this.app.get('port'))
        })
    }
}

const server = new Server();
server.start();